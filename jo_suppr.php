<!doctype html>
<html>
<head>
   <title>Supprimer un joueur</title>
   <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
// Connexion au serveur et à la base
$handle = mysqli_connect("localhost","root","1234","football");// Permet d'afficher la ligne ou se trouve l'erreur (optionnel)
ini_set("display_errors","true");// On contrôle que le paramètre id est défini et que c’est bien un nombre
if (isset($_GET["id"]) && is_numeric($_GET["id"])) {    // On récupère des informations sur le joueur
   $query = "SELECT * FROM joueurs WHERE id = " . $_GET["id"];
   $result = mysqli_query($handle,$query);    // On vérifie que l'équipe demandé existe
   if ($result->num_rows > 0) {        // On récupère l'équipe en elle-même
       $line = mysqli_fetch_array($result);
       $joueur_a_supprimer = $line["nom"];        // On effectue la suppression
       $query = "DELETE FROM joueurs WHERE id = " . $_GET["id"];
       $result = mysqli_query($handle,$query);        // On vérifie que tout s’est bien passé
       if ($handle->affected_rows > 0) {            echo "Le joueur " . $joueur_a_supprimer . " supprimé<br>\n";
       }
       else {
           echo "Le joueur " . $joueur_a_supprimer . " existe mais la suppression n’a pas marché<br>\n";
       }
   }
   else {
       echo "Le joueur demandé n’existe pas<br>\n";
   }
}
else {
   echo "Veuillez indiquer la variable id ou vérifier qu’il s’agit bien d’un nombre";
}
   echo "<a href=\"index.php\">retour</a>";
?></body>
</html>
